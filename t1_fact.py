from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import time


# Задание 1
# Создайте функцию по вычислению факториала числа. Запустите несколько задач, используя
# ThreadPoolExecutor и замерьте скорость их выполнения, а затем замерьте скорость вычисления,
# используя тот же самый набор задач на ProcessPoolExecutor. В качестве примеров, используйте
# крайние значения, начиная от минимальных и заканчивая максимально возможными, чтобы
# увидеть прирост или потерю производительности.

def factorial(started=0, finished=0):
    fact = 1
    for i in range(started, finished):
        fact *= i
    return fact


def executor_map(excl):
    executor = excl()
    started = time.time()
    params = [
        [1, 10],
        [5, 10]
    ]
    result = sum(executor.map(factorial, *params))
    print('Result: {result}. Time for {executor}: {spent_time}'.format(
        result=result,
        executor=excl.__name__,
        spent_time=time.time() - started
    ))


executor_map(ThreadPoolExecutor)
executor_map(ProcessPoolExecutor)
