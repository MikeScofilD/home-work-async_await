import asyncio


# Задание 3
# Разработайте сокет сервер на основе библиотеки asyncio. Сокет сервер должен выполнять
# сложение двух чисел, как из предыдущего примера по многопоточности.


async def sum2num(a, b):
    await asyncio.sleep(5)
    return a + b


async def async_sum2_num():
    result = await asyncio.gather(
        sum2num(5, 5),
        sum2num(6, 7),
        sum2num(9, 15),
    )

    print(result)


event_loop = asyncio.get_event_loop()
task_list = [
    event_loop.create_task(async_sum2_num())
]
tasks = asyncio.wait(task_list)
event_loop.run_until_complete(tasks)
event_loop.close()
