import os
import threading
import time


# Задание 2
# Создайте три функции,
# 1)одна из которых читает файл на диске с заданным именем и проверяет наличие строку “Wow! ”.
#   В случае, если файла нет, то засыпает на 5 секунд, а затем снова продолжает поиск по файлу.
#   В случае, если файл есть, то открывает его и ищет строку “Wow!”.
# При наличии данной строки закрывает файл и генерирует событие,
# а другая функция ожидает данное событие
# и в случае его возникновения выполняет удаление этого файла.
# В случае если строки «Wow!» не было найдено в файле,
# то засыпать на 5 секунд. Создайте файл руками и проверьте выполнение программы.


def stream_sleep():
    time.sleep(5)


def read_file(file, word):
    with open(file, 'r') as f:
        print(f"Открываем файл {file}")
        for line in f:
            if word in line:
                print(line)
        f.close()
        os.remove(file)


def check_file(file, word):
    try:
        read_file(file, word)
    except IOError as e:
        print(f'Не удалось открыть файл {e.filename}')
        stream_sleep()
        check_file(file, word)
    else:
        print('Файл удален')


def main():
    # read_file('disc.txt', 'massa')
    # check_file('dic.txt', 'massa')
    check_file('disc.txt', 'massa')


if __name__ == '__main__':
    main()
